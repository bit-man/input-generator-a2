input-generator-a2
==================

Generador de topología para Tp2 de Algoritmos 2 (FIUBA, Electrónicos, 2do cuatrimestre 2014)

Para poder probar el TP con una gran cantidad de nodos de red realizamos un generador de 
entradas automático (Input Generator) para facilitar la tarea.
La idea básica es generar un árbol con muchos nodos que respete las normas establecidas para
lo que significa, en este contexto, una red bien formada.
Elegimos realizar una implementación bastante simple en la cual los cable modems se conectan 
a amplificadores, estos a los nodos y estos al Hub. la forma de construirlo es agrupar los 
tipos de hijos en forma equitativa entre los tipo de padres. Si bien esto no ofrece una gran
flexibilidad si nos permite generar una gran cantidad de nodos y eso es lo importante 
(entendemos que, para este caso, predomina la cantidad de nodos sobre la flexibilidad).

El código para este generador, principalmente, consiste de 2 clases :

* InputGenerator : es quien se encarga de la lógica de conexión de los nodos
* SingleInputCreator : es quien dados los nodos y conexiones genera el archivo

Esperamos que lo disfruten y les sea de utilidad

Testing
=======

Nos basamos mucho en realizar tests para mejorar no solo la calidad del software
sino también para pder ir creciendo sobre una base sólida. En este caso utilizamos
[CppTest](http://cpptest.sourceforge.net/) simplemente por familiaridad son la misma. 
Para ejecutar los tests :


    $ make clean test && bin/ig-test.exe 

El código de los mismos puede encontrarse bajo "src/test"
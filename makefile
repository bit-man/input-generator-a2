LFLAGS=-Wall
CFLAGS=-Wall -pedantic -c 
C++_SOURCES := main.cpp InputGenerator.cpp SingleInputCreator.cpp
 
OBJS := src/InputGenerator.o src/SingleInputCreator.o src/main.o

EXECUTABLE := bin/ig.exe

## ToDo auto-discovery de includes ??
INCLUDES := -I /usr/include 
RM := rm -rf

all: ig

ig: compile-main build-exe

compile-main:
	@echo 'Building target: $@'
	@echo 'Invoking: Compiler'
	cd src && gcc ${CFLAGS} ${C++_SOURCES} ${INCLUDES}

build-exe:
	@echo 'Invoking: Linker'
	g++  -o  $(EXECUTABLE)  ${LFLAGS} $(OBJS)
	@echo 'Finished building target: $@'
	@echo ' '	
	
test: compile-main
	cd src/test && make all
	
clean:
	cd src/test && make clean
	-$(RM) $(EXECUTABLE) $(OBJS) $(TEST_OBJS) $(MAIN_OBJ)
	-@echo ' '


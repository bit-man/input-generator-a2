/*
 * InputGenerator.cpp
 *
 *  Created on: Apr 10, 2014
 */

#include "InputGenerator.h"

const string InputGenerator::NODE = string("NODE");
const string InputGenerator::HUB = string("HUB");
const string InputGenerator::AMP = "AMP";
const string InputGenerator::CM = "CM";


InputGenerator::InputGenerator( unsigned int numHubs, unsigned int numNodes, unsigned int numAmps,
                                unsigned long int numCMs )
{
    this->numHubs = numHubs;
    this->numNodes = numNodes;
    this->numAmps = numAmps;
    this->numCMs = numCMs;
}

InputGenerator::~InputGenerator() {
}

void InputGenerator::generateName() {
    this->netName = "pepito";
}

void InputGenerator::generateHubs() {
    nameGenerator(HUB, numHubs, hubs);
}

void InputGenerator::generateNodes() {
    nameGenerator(NODE, numNodes, nodes);
}

void InputGenerator::generateAmps() {
    nameGenerator(AMP, numAmps, amps);
}

void InputGenerator::generateCMs() {
    nameGenerator(CM, numCMs, cms);
}

void InputGenerator::generateConnects()
{
    if (numHubs == 0) {
        return;
    }

    /**
     * ToDo
     * usando esta relación hay un resto que no se contabiliza con la potencial pérdida
     * de network elements sobre el final de la última iteración. Dos soluciones que se me ocurren :
     * - acumular el resto de la división en cada iteración (quedan disminados equitativamente)
     * - hacer una iteración final con los restos de todos los network elements (potencial problema
     *   cuando hay 0 de resto + un grupo queda diseminado desparejo)
     *   ESTO PARA NUMEROS GRANDES NO IMPORTA (que es para lo que se va a usar, para generar volumen)
     */

    unsigned int nodeHubRatio = floor(numNodes / numHubs);
    unsigned int ampNodeRatio = (numNodes == 0) ? 0 : floor(numAmps / numNodes);
    unsigned long int cmAmpRatio;
    if (numAmps == 0)  {
        cmAmpRatio = 0;
    } else {
        cmAmpRatio = floor(numCMs / numAmps);
    }
    unsigned int cmNodeRatio = (numAmps == 0) ? 0 : floor(numCMs / numAmps);

    if (cmNodeRatio > 2000) {
        cerr << "There's more than 2000 cable modems (CM) per node (Node)" << endl;
        // ToDo throw exception
        exit(-1);
    }

    for (unsigned int hubNum = 0; hubNum < numHubs; hubNum++) {
        for (unsigned int nodeNum = hubNum * nodeHubRatio; nodeNum < (hubNum + 1) * nodeHubRatio;
                nodeNum++) {
            createConnection(NODE, nodeNum, HUB, hubNum);
            for (unsigned int ampNum = nodeNum * ampNodeRatio;
                    ampNum < (nodeNum + 1) * ampNodeRatio; ampNum++) {
                createConnection(AMP, ampNum, NODE, nodeNum);
                for (unsigned int cmNum = ampNum * cmAmpRatio;
                        cmNum < (ampNum + 1) * cmAmpRatio; cmNum++) {
                    createConnection(CM, cmNum, AMP, ampNum);
                }
            }
        }
    }
}

void InputGenerator::createConnection(const string &sonPrefix, unsigned int & sonNum, const  string &fatherPrefix, unsigned int &fatherNum ) {
    string son(createName(sonPrefix, sonNum));
    string father(createName(fatherPrefix, fatherNum));
    connections.push_back(Tuple<string, string>(son, father));
}

void InputGenerator::nameGenerator(const string & prefix, unsigned long int numElems, list<string> &container) {
    for (unsigned int i = 0; i < numElems; i++ ) {;
        container.push_back(createName(prefix,i));
    }
}

string InputGenerator::createName(const string& prefix, unsigned int index) {
    stringstream ss;
    ss <<"_" << prefix << "_" << index;
    return ss.str();
}

void InputGenerator::run( ostream& f )
{
    generateName();
    generateHubs();
    generateNodes();
    generateAmps();
    generateCMs();
    generateConnects();

    SingleInputCreator ic(this->netName, this->hubs, this->nodes, this->amps, this->cms,
                          this->connections);
    ic.run(f);
}


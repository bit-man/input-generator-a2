/*
 * SingleInputCreator.cpp
 *
 *  Created on: Apr 10, 2014
 */

#include "SingleInputCreator.h"

SingleInputCreator::SingleInputCreator( string netName, list<string> hubs, list<string> nodes,
                                        list<string> amps, list<string> cms,
                                        list< Tuple<string,string> > connections )
{
    this->netName = netName;
    this->hubs = hubs;
    this->nodes = nodes;
    this->amps = amps;
    this->cms = cms;
    this->connections = connections;
}

SingleInputCreator::~SingleInputCreator() {
}

void SingleInputCreator::createElements(list<string> & elems, const char * elemName, const char *type, ostream& f) {
	for (list<string>::iterator it = elems.begin(); it != elems.end(); ++it)
		f << elemName << " " << *it << " " << type << endl;
}

void SingleInputCreator::createConnections(ostream & f) {

	list< Tuple<string,string> >::iterator it(this->connections.begin());
	while( it != this->connections.end()) {
	    // Connection HIJO PADRE
	    f << "Connection " << it->first << " " << it->second << endl;
        it++;
	}

}

void SingleInputCreator::run(ostream &f) {
    f << "NetworkName " << this->netName << endl;

	createElements(this->hubs, "NetworkElement", "Hub", f);
	createElements(this->nodes, "NetworkElement", "Node", f);
	createElements(this->amps, "NetworkElement", "Amp", f);
	createElements(this->cms, "NetworkElement", "CM", f);

	createConnections(f);

}



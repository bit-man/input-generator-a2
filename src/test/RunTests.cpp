/*
 * RunTests.cpp
 *
 *  Created on: Apr 10, 2014
 */

using namespace std;

#include <cpptest.h>
#include <iostream>
#include <sstream>
#include <SingleInputCreator.h>
#include <InputGenerator.h>
#include <Tuple.h>

class SICTestSuite: public Test::Suite {
public:
	SICTestSuite() {
		TEST_ADD(SICTestSuite::justName)
		TEST_ADD(SICTestSuite::nameAndHubs)
		TEST_ADD(SICTestSuite::nameAndNodes)
		TEST_ADD(SICTestSuite::nameAndAmps)
		TEST_ADD(SICTestSuite::nameAndCMs)
		TEST_ADD(SICTestSuite::numHubsConn1)
		TEST_ADD(SICTestSuite::nameNodesConn1)
		TEST_ADD(SICTestSuite::nameAmpsConn1)
		TEST_ADD(SICTestSuite::nameCMsConn1)
		TEST_ADD(SICTestSuite::nameCMsConn2)
	}

private:
	void justName();
	void nameAndHubs();
	void nameAndNodes();
	void nameAndAmps();
	void nameAndCMs();
	void numHubsConn1();
	void nameNodesConn1();
	void nameAmpsConn1();
	void nameCMsConn1();
	void nameCMsConn2();
};

void SICTestSuite::justName()
{
    string netName("pepito");
    list<string> hubs;
    list<string> nodes;
    list<string> amps;
    list<string> cms;
    list< Tuple<string,string> > connections;

    stringstream out;
    SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
    sic.run(out);

    string s;
    getline(out, s);
    TEST_ASSERT_EQUALS("NetworkName pepito", s);
}

void SICTestSuite::nameAndHubs() {
	string netName("pepito");
	string myHubs[] = { "AAAA", "BBBB" };
	list<string> hubs(myHubs, myHubs + 2);
	list<string> nodes;
	list<string> amps;
	list<string> cms;
	list< Tuple<string,string> >connections;
    stringstream out;
    string s;

	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	s = out.str();
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement AAAA Hub\nNetworkElement BBBB Hub\n",
			s);
}

void SICTestSuite::nameAndNodes() {
	string netName("pepito");
	list<string> hubs;
	string elems[] = { "AAAA", "BBBB" };
	list<string> nodes(elems, elems + 2);
	list<string> amps;
	list<string> cms;
	list< Tuple<string,string> > connections;
	stringstream out;


	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement AAAA Node\nNetworkElement BBBB Node\n",
			out.str());
}

void SICTestSuite::nameAndAmps() {
	string netName("pepito");
	list<string> hubs;
	list<string> nodes;
	string elems[] = { "AAAA", "BBBB" };
	list<string> amps(elems, elems + 2);
	list<string> cms;
	list< Tuple<string,string> > connections;
    stringstream out;

	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement AAAA Amp\nNetworkElement BBBB Amp\n",
			out.str());
}

void SICTestSuite::nameAndCMs() {
	string netName("pepito");
	list<string> hubs;
	list<string> nodes;
	list<string> amps;
	string elems[] = { "AAAA", "BBBB" };
	list<string> cms(elems, elems + 2);
	list< Tuple<string,string> > connections;
    stringstream out;

	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement AAAA CM\nNetworkElement BBBB CM\n",
			out.str());
}

void SICTestSuite::numHubsConn1() {
	string netName("pepito");
	string myHubs[] = { "AAAA", "BBBB" };
	list<string> hubs(myHubs, myHubs + 2);
	list<string> nodes;
	list<string> amps;
	list<string> cms;
	list< Tuple<string,string> > connections;
    stringstream out;


    string son("BBBB");
    string father("AAAA");
    connections.push_back( Tuple<string,string>( son, father) );
	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement AAAA Hub\nNetworkElement BBBB Hub\nConnection BBBB AAAA\n",
			out.str());
}


void SICTestSuite::nameNodesConn1() {
	string netName("pepito");
	list<string> hubs;
	string elems[] = { "CCCC", "DDDD" };
	list<string> nodes(elems, elems + 2);
	list<string> amps;
	list<string> cms;
	list< Tuple<string,string> > connections;
    stringstream out;

    string son("CCCC");
    string father("DDDD");
    connections.push_back( Tuple<string,string>( son, father) );
	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement CCCC Node\nNetworkElement DDDD Node\nConnection CCCC DDDD\n",
			out.str());
}

void SICTestSuite::nameAmpsConn1() {
	string netName("pepito");
	list<string> hubs;
	list<string> nodes;
	string elems[] = { "EEEE", "FFFF" };
	list<string> amps(elems, elems + 2);
	list<string> cms;
	list< Tuple<string,string> > connections;
    stringstream out;


    string son("FFFF");
    string father("EEEE");
    connections.push_back( Tuple<string,string>( son, father) );

	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement EEEE Amp\nNetworkElement FFFF Amp\nConnection FFFF EEEE\n",
			out.str());
}

void SICTestSuite::nameCMsConn1() {
	string netName("pepito");
	list<string> hubs;
	list<string> nodes;
	list<string> amps;
	string elems[] = { "GGGG", "HHHH" };
	list<string> cms(elems, elems + 2);
	list< Tuple<string,string> > connections;
    stringstream out;

    string son("HHHH");
    string father("GGGG");
    connections.push_back( Tuple<string,string>( son, father) );
	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);
	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement GGGG CM\nNetworkElement HHHH CM\nConnection HHHH GGGG\n",
			out.str());
}


void SICTestSuite::nameCMsConn2() {
	string netName("pepito");
	list<string> hubs;
	list<string> nodes;
	list<string> amps;
	string elems[] = { "GGGG", "HHHH" };
	list<string> cms(elems, elems + 2);
	list< Tuple<string,string> > connections;
    stringstream out;

    string son("HHHH");
    string son2("KKKK");
    string father("GGGG");

    connections.push_back(Tuple<string,string>( son, father) );
    connections.push_back( Tuple<string,string>( son2, father) );
	SingleInputCreator sic(netName, hubs, nodes, amps, cms, connections);
	sic.run(out);

	TEST_ASSERT_EQUALS(
			"NetworkName pepito\nNetworkElement GGGG CM\nNetworkElement HHHH CM\nConnection HHHH GGGG\nConnection KKKK GGGG\n",
			out.str());
}


class TupleTestSuite: public Test::Suite {
public:
        TupleTestSuite() {
            TEST_ADD(TupleTestSuite::equals)
            TEST_ADD(TupleTestSuite::less)
    }

private:
    void equals();
    void less();
};

void TupleTestSuite::equals()
{
    string son("HHHH");
    string son2("KKKK");
    string father("GGGG");

    Tuple<string,string> tp(son, son2);
    Tuple<string,string> tp2(son, son2);
    Tuple<string,string> tp3(son, father);
    TEST_ASSERT(tp == tp2);
    TEST_ASSERT(tp != tp3);
    TEST_ASSERT_EQUALS(true, tp != tp3);
    TEST_ASSERT_EQUALS(true, tp == tp2);
}


void TupleTestSuite::less()
{
    string son("HHHH");
    string son2("KKKK");
    string father("GGGG");

    Tuple<string,string> tp(son, son2);
    Tuple<string,string> tp2(son, son2);
    Tuple<string,string> tp3(son, father);
    TEST_ASSERT(tp == tp2);
    TEST_ASSERT(tp != tp3);
    TEST_ASSERT_EQUALS(true, tp != tp3);
    TEST_ASSERT_EQUALS(true, tp == tp2);
}

class IGTestSuite: public Test::Suite {
public:
        IGTestSuite() {
            TEST_ADD(IGTestSuite::noHub)
            TEST_ADD(IGTestSuite::hub)
            TEST_ADD(IGTestSuite::node)
            TEST_ADD(IGTestSuite::amp)
            TEST_ADD(IGTestSuite::cm)
            TEST_ADD(IGTestSuite::connHubNode)
            TEST_ADD(IGTestSuite::conn2Hub2Node)
            TEST_ADD(IGTestSuite::conn2Hub3Node)
            TEST_ADD(IGTestSuite::conn2Hub2Node2amp)
            TEST_ADD(IGTestSuite::conn2Hub2Node2amp2cm)
    }

private:
        void noHub();
        void hub();
        void node();
        void amp();
        void cm();
        void connHubNode();
        void conn2Hub2Node();
        void conn2Hub3Node();
        void conn2Hub2Node2amp();
        void conn2Hub2Node2amp2cm();
};


void IGTestSuite::noHub() {
    stringstream out;
    InputGenerator ig(0,0,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\n",
            out.str());
}

void IGTestSuite::hub() {
    stringstream out;
    InputGenerator ig(2,0,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _HUB_1 Hub\n",
            out.str());
}

void IGTestSuite::node() {
    stringstream out;
    InputGenerator ig(0,2,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _NODE_0 Node\nNetworkElement _NODE_1 Node\n",
            out.str());
}

void IGTestSuite::amp() {
    stringstream out;
    InputGenerator ig(0,0,2,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _AMP_0 Amp\nNetworkElement _AMP_1 Amp\n",
            out.str());
}

void IGTestSuite::cm() {
    stringstream out;
    InputGenerator ig(0,0,0,2);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _CM_0 CM\nNetworkElement _CM_1 CM\n",
            out.str());
}

void IGTestSuite::connHubNode() {
    stringstream out;
    InputGenerator ig(1,1,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _NODE_0 Node\nConnection _NODE_0 _HUB_0\n",
            out.str());
}

void IGTestSuite::conn2Hub2Node() {
    stringstream out;
    InputGenerator ig(2,2,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _HUB_1 Hub\nNetworkElement _NODE_0 Node\nNetworkElement _NODE_1 Node\nConnection _NODE_0 _HUB_0\nConnection _NODE_1 _HUB_1\n",
            out.str());
}

void IGTestSuite::conn2Hub3Node() {
    stringstream out;
    InputGenerator ig(2,3,0,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _HUB_1 Hub\nNetworkElement _NODE_0 Node\nNetworkElement _NODE_1 Node\nNetworkElement _NODE_2 Node\nConnection _NODE_0 _HUB_0\nConnection _NODE_1 _HUB_1\n",
            out.str());
}

void IGTestSuite::conn2Hub2Node2amp() {
    stringstream out;
    InputGenerator ig(2,2,2,0);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _HUB_1 Hub\nNetworkElement _NODE_0 Node\nNetworkElement _NODE_1 Node\nNetworkElement _AMP_0 Amp\nNetworkElement _AMP_1 Amp\nConnection _NODE_0 _HUB_0\nConnection _AMP_0 _NODE_0\nConnection _NODE_1 _HUB_1\nConnection _AMP_1 _NODE_1\n",
            out.str());
}

void IGTestSuite::conn2Hub2Node2amp2cm()
{
    stringstream out;
    InputGenerator ig(2, 2, 2, 2);
    ig.run(out);

    TEST_ASSERT_EQUALS(
            "NetworkName pepito\nNetworkElement _HUB_0 Hub\nNetworkElement _HUB_1 Hub\nNetworkElement _NODE_0 Node\nNetworkElement _NODE_1 Node\nNetworkElement _AMP_0 Amp\nNetworkElement _AMP_1 Amp\nNetworkElement _CM_0 CM\nNetworkElement _CM_1 CM\nConnection _NODE_0 _HUB_0\nConnection _AMP_0 _NODE_0\nConnection _CM_0 _AMP_0\nConnection _NODE_1 _HUB_1\nConnection _AMP_1 _NODE_1\nConnection _CM_1 _AMP_1\n",
            out.str());
}

int main()
{
    Test::TextOutput output(Test::TextOutput::Verbose);
    SICTestSuite ets;
    TupleTestSuite tts;
    IGTestSuite igts;

    return !(ets.run(output) & tts.run(output) & igts.run(output));
}

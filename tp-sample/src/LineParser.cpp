
#include "LineParser.h"

LineParser::LineParser(const string line) {
	this->line = line;
	this->params = new vector<string>();
	this->command = "";
}

LineParser::LineParser(const LineParser & lp) {
	this->line = lp.line;
	this->params = new vector<string>(*lp.params);
	this->command = lp.command;
}

LineParser::~LineParser() {
	if (this->params != NULL)
		delete this->params;
}

void LineParser::trimEnd(string &s) {
	string whitespaces (" \t\f\v\n\r");

	size_t found = s.find_last_not_of(whitespaces);
	if (found != string::npos) {
		s.erase(found+1);
	}
}

void LineParser::parse(){
	string cleanS(this->line);
	trimEnd(cleanS);

	stringstream ss(cleanS);

	ss >> this->command;

	/**
	 * SPEC : ... Asimismo, por tratarse de un archivo de texto,
	 * la cantidad de espacios entre tokens puede ser variable
	 */
	string s;
	while( ! ss.eof() && ss.good() ) {
		ss >> s;
		this->params->push_back(s);
	}

}

vector<string> *LineParser::getParams() const {
	return this->params;
}

string LineParser::getCommand() const {
	return this->command;
}




#ifndef LINEPARSER_H_
#define LINEPARSER_H_

using namespace std;

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class LineParser {
private:
	string line;
	string command;
	vector<string> *params;

	void trimEnd(string &);

public:
	LineParser(const string );
	LineParser(const LineParser & );
	virtual ~LineParser();
	void parse();
	string getCommand() const;
	vector<string> * getParams() const;
};

#endif /* LINEPARSER_H_ */


#include "LineParserTestSuite.h"

void LineParserTestSuite::comandoUnico() {
	LineParser *lp = new LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoSinParams() {
	LineParser *lp = new LineParser("AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoConEspacio() {
	LineParser *lp = new LineParser(" AAAA");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoConEspacioAlfinal() {
	LineParser *lp = new LineParser("AAAA ");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	delete lp;
}

void LineParserTestSuite::comandoParametro() {
	LineParser *lp = new LineParser("AAAA bbbbb");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(1, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	delete lp;
}

void LineParserTestSuite::comando2Parametros() {
	LineParser *lp = new LineParser("AAAA bbbbb ccc");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(2, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	TEST_ASSERT_EQUALS("ccc", (*p)[1]);
	delete lp;
}

void LineParserTestSuite::comandoConEspaciosAlFinal() {
	LineParser *lp = new LineParser("AAAA bbbbb ccc  ");
	lp->parse();

	TEST_ASSERT_EQUALS("AAAA", lp->getCommand());
	vector<string> *p = lp->getParams();
	TEST_ASSERT_EQUALS(2, p->size());
	TEST_ASSERT_EQUALS("bbbbb", (*p)[0]);
	TEST_ASSERT_EQUALS("ccc", (*p)[1]);
	delete lp;
}


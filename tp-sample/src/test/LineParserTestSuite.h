
#ifndef LINEPARSERTESTSUITE_H_
#define LINEPARSERTESTSUITE_H_

#include "../LineParser.h"
#include <cpptest.h>

class LineParserTestSuite: public Test::Suite {
public:
	LineParserTestSuite() {
		TEST_ADD(LineParserTestSuite::comandoUnico)
		TEST_ADD(LineParserTestSuite::comandoSinParams)
		TEST_ADD(LineParserTestSuite::comandoConEspacio)
		TEST_ADD(LineParserTestSuite::comandoConEspacioAlfinal)
		TEST_ADD(LineParserTestSuite::comandoParametro)
		TEST_ADD(LineParserTestSuite::comando2Parametros)
		TEST_ADD(LineParserTestSuite::comandoConEspaciosAlFinal)
	}

private:
	void comandoUnico();
	void comandoSinParams();
	void comandoConEspacio();
	void comandoConEspacioAlfinal();
	void comandoParametro();
	void comando2Parametros();
	void comandoConEspaciosAlFinal();
};

#endif /* LINEPARSERTESTSUITE_H_ */

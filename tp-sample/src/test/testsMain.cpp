
#include "LineParserTestSuite.h"

int main(int argc, char *argv[]) {
	Test::TextOutput output(Test::TextOutput::Verbose);
	LineParserTestSuite ets;
	
	// Si hay errores en los tests terminar con error (devuelve 1)
	return ! ets.run(output);
}



